import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class Digitalid extends StatefulWidget {
  @override
  _DigitalidState createState() => _DigitalidState();
}

class _DigitalidState extends State<Digitalid> {
  Widget _buildQrCode(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: QrImage(
        backgroundColor: Colors.deepPurple.shade50,
        foregroundColor: Colors.black87,
        padding: EdgeInsets.all(20),
        size: 250,
        data: "https://youtu.be/QMyYWh1b_kU",
      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.deepPurple.shade50,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildGetDetails(BuildContext context) {
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        padding: EdgeInsets.only(top: 8.0),
        child: Column(
          children: <Widget>[
            Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Colors.deepPurple.shade900,
                  ),
                  title: Text(
                    'khairulshukeri@tnb.com.my',
                    style: TextStyle(
                      color: Colors.black87,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                    ),
                  ),
                )),
            Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    color: Colors.deepPurple.shade900,
                  ),
                  title: Text(
                    '03-34343434',
                    style: TextStyle(
                      color: Colors.black87,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                    ),
                  ),
                )),
            Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.phone_iphone,
                    color: Colors.deepPurple.shade900,
                  ),
                  title: Text(
                    '017-5587882',
                    style: TextStyle(
                      color: Colors.black87,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                    ),
                  ),
                )),
          ],
        ));
  }

  Widget _buildButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: RaisedButton(
        onPressed: () {},
        child: new Text(
          'Edit',
          style: TextStyle(color: Colors.white70),
        ),
        color: Colors.deepPurple.shade500,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: AppBar(
        title: new Text("kita TNB"),
        backgroundColor: Colors.deepPurple.shade900,
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: screenSize.height / 15.0),
                  _buildQrCode(context),
                  _buildSeparator(screenSize),
                  SizedBox(height: 10.0),
                  _buildGetDetails(context),
                  SizedBox(height: 8.0),
                  _buildButtons(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
