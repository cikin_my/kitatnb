import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: new Text(
          '...Add Login page here',
          style: TextStyle(
              color: Colors.black54,
              fontSize: 23,
              fontWeight: FontWeight.w800,
              shadows: [
                Shadow(
                  blurRadius: 10.0,
                  color: Colors.grey.shade300,
                  offset: Offset(5.0, 5.0),
                ),
              ]),
        ),
      ),
    );
  }
}
