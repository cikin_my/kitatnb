import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:kitatnb/Pages/news.dart';
import 'package:intl/intl.dart';

class DiscoverPage extends StatefulWidget {
  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  final FlutterWebviewPlugin flutterWebviewPlugin = new FlutterWebviewPlugin();
  static const String placeholderImg = 'assets/images/png/news.png';
  bool loadingInProgress;
  var newslist;

  void getNews() async {
    News news = News();
    await news.getNews();
    setState(() {
      loadingInProgress = false;
      newslist = news.news;
    });
  }

  @override
  void initState() {
    loadingInProgress = true;
    // TODO: implement initState
    super.initState();
    getNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20.0,
        ),
        child: loadingInProgress
            ? Center(
                child: Text('loading ...'),
              )
            : Column(
                children: <Widget>[
//                Row(
//                  children: <Widget>[
//                    Text(
//                      'Top HeadLines',
//                      style: TextStyle(
//                        fontSize: 30.0,
//                        letterSpacing: 2,
//                        fontWeight: FontWeight.bold,
//                      ),
//                    )
//                  ],
//                ),
//                Divider(),
                  Expanded(
                    child: ListView.builder(
                        padding: new EdgeInsets.all(8.0),
                        itemCount: newslist.length,
                        itemBuilder: (context, index) {
//                  UI design start here
                          return new Card(
                              elevation: 1.7,
                              child: new Padding(
                                padding: new EdgeInsets.all(10.0),
                                child: new Column(
                                  children: [
                                    //======== date row ==========
                                    new Row(
                                      children: <Widget>[
                                        new Padding(
                                          padding:
                                              new EdgeInsets.only(left: 4.0),
                                          child: new Text(
                                            DateFormat.Hm().format(
                                                newslist[index].publishedAt),
                                            style: new TextStyle(
                                              fontWeight: FontWeight.w400,
                                              color: Colors.grey[600],
                                            ),
                                          ),
                                        ),
                                        new Padding(
                                          padding: new EdgeInsets.all(5.0),
                                          child: new Text(
                                            newslist[index]["source"]["name"],
                                            style: new TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: Colors.grey[700],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //========== news row ========
                                    new Row(
                                      children: [
                                        new Expanded(
                                          child: new GestureDetector(
                                            child: new Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
//                                t i t l e
                                                new Padding(
                                                  padding: new EdgeInsets.only(
                                                      left: 4.0,
                                                      right: 8.0,
                                                      bottom: 8.0,
                                                      top: 8.0),
                                                  child: new Text(
                                                    newslist[index].title,
                                                    style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
//                                d e s c r i p t i o n
                                                new Padding(
                                                  padding: new EdgeInsets.only(
                                                      left: 4.0,
                                                      right: 4.0,
                                                      bottom: 4.0),
                                                  child: new Text(
                                                    newslist[index].description,
                                                    style: new TextStyle(
                                                      color: Colors.grey[500],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            onTap: () {
                                              flutterWebviewPlugin
                                                  .launch(newslist[index].url);
                                            },
                                          ),
                                        ),
                                        new Column(
                                          children: <Widget>[
                                            new Padding(
                                              padding:
                                                  new EdgeInsets.only(top: 8.0),
                                              child: new SizedBox(
                                                height: 100.0,
                                                width: 100.0,
                                                child: CachedNetworkImage(
                                                  placeholder: (context, url) =>
                                                      Image.asset(
                                                          placeholderImg),
                                                  imageUrl: newslist[index]
                                                      .urlToImage,
                                                  height: 50,
                                                  width: 70,
                                                  alignment: Alignment.center,
                                                  fit: BoxFit.cover,
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Image.asset(
                                                    'assets/images/png/news.png',
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ));
                        }),
                  )
                ],
              ),
      )),
    );
  }
}
