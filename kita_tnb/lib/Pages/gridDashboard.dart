import 'package:flutter/material.dart';

class GridDashboard extends StatelessWidget {
  Items item1 = new Items(
      title: "Address Book",
      subtitle: "My Digital Contact ",
      event: "",
      img: "assets/images/numbers.png");

  Items item2 = new Items(
    title: "Scan",
    subtitle: "Scan other Digital ID",
    event: "",
    img: "assets/images/qr-code.png",
  );
  Items item3 = new Items(
    title: "Check In",
    subtitle: "WFH Daily check in",
    event: "",
    img: "assets/images/calendar.png",
  );
  Items item4 = new Items(
    title: "Attendance",
    subtitle: "My WFH attendance record",
    event: "",
    img: "assets/images/attendance.png",
  );
  Items item5 = new Items(
    title: "Milestone",
    subtitle: "Weekly Target Plan",
    event: "",
    img: "assets/images/milestone.png",
  );
  Items item6 = new Items(
    title: "Daily Planner",
    subtitle: "daily planner Key Activity",
    event: "",
    img: "assets/images/to-do-list.png",
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3, item4, item5, item6];
    var color = 0xFFF5F5F5;
    return Flexible(
      child: GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return Container(
              decoration: BoxDecoration(
                  color: Color(color),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[200],
                      blurRadius: 2.0, // has the effect of softening the shadow
                      spreadRadius:
                          1.0, // has the effect of extending the shadow
                      offset: Offset(
                        3.0, // horizontal, move right 10
                        3.0, // vertical, move down 10
                      ),
                    )
                  ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    data.img,
                    width: 42,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    data.title,
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    data.subtitle,
                    style: TextStyle(
                        color: Colors.black45,
                        fontSize: 10,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    data.event,
                    style: TextStyle(
                        color: Colors.black38,
                        fontSize: 11,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            );
          }).toList()),
    );
  }
}

class Items {
  String title;
  String subtitle;
  String event;
  String img;

  Items({this.title, this.subtitle, this.event, this.img});
}
