import 'package:flutter/material.dart';

class MorePage extends StatefulWidget {
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: new Text(
          "more page",
          style: TextStyle(
              color: Colors.black54,
              fontSize: 23,
              fontWeight: FontWeight.w800,
              shadows: [
                Shadow(
                  blurRadius: 10.0,
                  color: Colors.grey.shade300,
                  offset: Offset(5.0, 5.0),
                ),
              ]),
        ),
      ),
    );
  }
}
