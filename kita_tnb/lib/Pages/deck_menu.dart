import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DeckMenu extends StatefulWidget {
  @override
  _DeckMenuState createState() => _DeckMenuState();
}

class _DeckMenuState extends State<DeckMenu> {
  List<String> events = [
    "Kemaskini",
    "Scan",
    "Check In",
    "Attendance",
    "Milestone",
    "Daily Planner",
  ];

  @override
  Widget build(BuildContext context) {
    var color = 0xFFF5F5F5;
    return Flexible(
      child: GridView(
        padding: EdgeInsets.only(left: 16, right: 16),
        physics: BouncingScrollPhysics(), //only for ios
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: events.map((title) {
          return GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  color: Color(color),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[200],
                      blurRadius: 2.0, // has the effect of softening the shadow
                      spreadRadius:
                          1.0, // has the effect of extending the shadow
                      offset: Offset(
                        3.0, // horizontal, move right 10
                        3.0, // vertical, move down 10
                      ),
                    )
                  ]),
              margin: const EdgeInsets.all(10.0),
              child: getCardByTitle(title),
            ),
            onTap: () {
              Fluttertoast.showToast(
                  msg: title + " will coming soon",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  backgroundColor: Colors.deepPurple.shade900,
                  textColor: Colors.white70,
                  fontSize: 16.0);
            },
          );
        }).toList(),
      ),
    );
  }
}

Column getCardByTitle(String title) {
  String img = "";
  String subtitle = "";
  if (title == "Kemaskini") {
    img = "assets/images/png/kemaskini.png";
    subtitle = "Maklumat Pasca PKP";
  } else if (title == "Scan") {
    img = "assets/images/png/qr-code.png";
    subtitle = "Scan other Digital ID";
  } else if (title == "Check In") {
    img = "assets/images/png/calendar.png";
    subtitle = "WFH Daily check in";
  } else if (title == "Attendance") {
    img = "assets/images/png/attendance.png";
    subtitle = "My WFH Record";
  } else if (title == "Milestone") {
    img = "assets/images/png/milestone.png";
    subtitle = "Weekly Target Plan";
  } else {
    img = "assets/images/png/to-do-list.png";
    subtitle = "daily planner Key Activity";
  }

  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        img,
        width: 45,
      ),
      SizedBox(
        height: 10,
      ),
      Text(
        title,
        style: TextStyle(
            color: Colors.black87, fontSize: 16, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 8,
      ),
      Text(
        subtitle,
        style: TextStyle(
            color: Colors.black45, fontSize: 12, fontWeight: FontWeight.w600),
      ),
    ],
  );
}
