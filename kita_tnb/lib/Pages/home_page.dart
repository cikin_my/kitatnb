import 'package:flutter/material.dart';
import 'package:kitatnb/Pages/digitalid.dart';
import 'package:kitatnb/Pages/profile.dart';
import 'package:kitatnb/Pages/discoverPage.dart';
import 'package:kitatnb/Pages/morePage.dart';
import 'package:kitatnb/Pages/myDeckPage.dart';
import 'package:kitatnb/main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 1;
  final List<Widget> _children = [
    DiscoverPage(),
    MyDeckPage(),
    MorePage(),
  ];

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _children[_currentIndex],
      appBar: AppBar(
        title: new Text(
          "kita TNB",
          style: TextStyle(color: Colors.white70),
        ),
        centerTitle: true,
        backgroundColor: Colors.deepPurple.shade900,
      ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text(
                "Shukeri Salleh",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w800,
                    shadows: [
                      Shadow(
                        blurRadius: 10.0,
                        color: Colors.black38,
                        offset: Offset(5.0, 5.0),
                      ),
                    ]),
              ),
              accountEmail: new Text(
                'Prototype Manager',
                style: TextStyle(
                    letterSpacing: 3.0,
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                    shadows: [
                      Shadow(
                        blurRadius: 10.0,
                        color: Colors.black38,
                        offset: Offset(5.0, 5.0),
                      ),
                    ]),
              ),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                    backgroundImage: new AssetImage("assets/images/me.jpg")),
                onTap: () => print("This is you"),
              ),
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: new AssetImage("assets/images/drawer_bg.jpg"))),
            ),
            new ListTile(
              trailing: new Icon(Icons.person),
              title: new Text("View my digital Profile"),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new Profile()));
              },
            ),
            new Divider(height: 1),
            new ListTile(
              trailing: new Icon(Icons.sim_card),
              title: new Text("View MyDigital ID"),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new Digitalid()));
              },
            ),
            new Divider(
              height: 1,
            ),
            new ListTile(
              trailing: new Icon(Icons.settings_power),
              title: new Text("Log Out"),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new StartPage()));
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.touch_app),
              title: new Text('Discover'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.collections_bookmark),
              title: new Text('My Deck'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.more),
              title: new Text('More'),
            ),
          ]),
    );
  }
}
